<?php
class Database {

    private $host = "localhost";
    private $db_name = "clienteless";
    private $password = "";
    private $username = "root";

    private $charset = "utf8mb4";

    private $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false
    ];

    private $pdo;
    private function connect(){
        $this->pdo = null;

        try {
            $this->pdo = new PDO("mysql:host=".$this->host."; dbname=".$this->db_name."", $this->username, $this->password, $this->options);


        } catch(\PDOException $e){
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        return $this->pdo;
    }

    public function dbconn(){
        return $this->connect();
    }
    
}

?>