<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Allow-Headers: X-Requested-With");
?>

<span class="login100-form-title p-b-55 p-t-55">
    Student Login
</span>

<div class="wrap-input100 m-b-16" >
    <input class="input100" type="text" name="username" placeholder="Username" id="in-user">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <span class="lnr lnr-user"></span>
    </span>
</div>

<div class="wrap-input100 m-b-16">
    <input class="input100" type="password" name="pass" placeholder="Password" id="in-pass">
    <span class="focus-input100"></span>
    <span class="symbol-input100">
        <span class="lnr lnr-lock"></span>
    </span>
</div>

<div class="contact100-form-checkbox m-l-4">
    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
    <label class="label-checkbox100" for="ckb1">
        Remember me
    </label>
</div>

<div class="container-login100-form-btn p-t-25">
    <button class="login100-form-btn" id="account_login">
        Login
    </button>
</div>


<div class="text-center w-full p-t-42 p-b-22">
    <span class="txt1">
        Not a Student?
    </span>
</div>

<button class="btn-face m-b-10" id="admin-login">
    Admin
</button>

<button class="btn-google m-b-10">
    SuperAdmin
</button>

<div class="text-center w-full p-t-115">
    <span class="txt1">
        Not a member?
    </span>

    <a class="txt1 bo1 hov1" href="#">
        Sign up now							
    </a>
</div>

<script>

    $(document).keypress(function (args) {
        if(args.which == 13){
            $("button#account_login").click()
        }
    })

    $("button#account_login").click(function() {
        var user = $("input#in-user").val()
        var pass = $("input#in-pass").val()

        let data = {
            user : user,
            pass : pass,
            default : 3
        }

        $.ajax({
            url : "api/post/loginRequest.php",
            method : "POST",
            data : data,
            success : response => {
                if(response.message){
                    alert(response.message)
                } else {
                    window.location = "views/user-landing.php"
                }
            },
            error : error => {
                console.log(error);
            }
        })
    })

    $("button#admin-login").click(function(){
        $.post("views/admin-login.html", data => {
            $("#form_body").fadeOut()
            $("#adm-form").fadeIn().html(`<div class="spinner-border m-l-r-auto" role="status">
  <span class="sr-only">Loading...</span>
</div>`)
            setTimeout(() => {
                $(".spinner-border").fadeOut()
                    $("#adm-form").html(data).fadeIn(5000)
            }, 1000);
        })
    })

    $("#loginModal").on('hidden.bs.modal', () => {
        $("#form_body").show()
        $("#adm-form").html("")
    })
</script>