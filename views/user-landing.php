<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();

    if (!isset($_SESSION['userID'])) {
        header("Location: ../index.php");
    }
}

include_once "header.html";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script src="../resources/js/app.js"></script>

    <title>Document</title>
</head>

<body>

    <div id="carouselExampleSlidesOnly" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="../resources/img/canteen.jpg" alt="First slide" style="height: 100vh; width: 100%">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../resources/img/admin.jpg" alt="Second slide" style="height: 100vh; width: 100%">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="../resources/img/clinic.jpg" alt="Third slide" style="height: 100vh; width: 100%">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="container-fluid top-layer-content">
        <div class="row m-t-50 w-bg-rgba">
            <div class="col-md-5 p-t-20">
                <div class="clearfix cl-al">
                    <div class="float-left">
                        <img src="../resources/img/survey.png" alt="template" style="height: 70px">
                    </div>
                    <div class="float-left">
                        <h1 class="flh100">
                            Evaluation Form
                        </h1>
                    </div>
                </div>
                <div class="container">
                    <p class="p-center">
                        We firmly believe that it is our duty to create an educational environment that each student is given the chance to live his student life to the fullest. That is why we will do what is expected of us to make your stay in Bicol University a happy and rewarding experience, to realize this, we want you to help us determine where our support services are strong and weak, so that we can sustain our strengths and improve our weaknesses, through improvement of the university.
                    </p>
                    <br>
                    <p class="p-center">
                        Please help us rate the performance of our support service by choosing the appropriate number, using the following rating scales.
                    </p>
                    <br>
                    <div class="row m-l-90 m-b-20">
                        <div class="col-md-6">
                            <ul>
                                <li>5 - Excellent</li>
                                <li>4 - Very Satisfactory</li>
                                <li>3 - Satisfactory</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>2 - Fair</li>
                                <li>1 - Poor</li>
                                <li>NC - No comment</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 p-t-30 p-l-50 p-r-50 p-b-20">
                <div id="survey-container">
                    <div class="form-group" id="1">
                        <label for="aoremarks" class="fs-20">I. Admission's Office</label>
                        <div class="form-row p-l-30">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">A. Quality Service Provided</label>
                                <label for="inputPassword4" class="m-b-10">B. Personal Attitude</label>
                            </div>
                            <div class="form-group col-md-6">
                                <form action="#" id="qsp" class="m-b-10">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp1" value="1">
                                        <label class="form-check-label" for="qsp1">1</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp2" value="2">
                                        <label class="form-check-label" for="qsp2">2</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp3" value="3">
                                        <label class="form-check-label" for="qsp3">3</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp4" value="4">
                                        <label class="form-check-label" for="qsp4">4</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp5" value="5">
                                        <label class="form-check-label" for="qsp5">5</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsQSP" id="qsp6" value="6">
                                        <label class="form-check-label" for="qsp6">NC</label>
                                    </div>
                                </form>
                                <form action="#" id="paao">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao1" value="1">
                                        <label class="form-check-label" for="inlineRadio1">1</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao2" value="2">
                                        <label class="form-check-label" for="paao2">2</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao3" value="3">
                                        <label class="form-check-label" for="paao3">3</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao4" value="4">
                                        <label class="form-check-label" for="paao4">4</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao5" value="5">
                                        <label class="form-check-label" for="paao5">5</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsPAAO" id="paao6" value="6">
                                        <label class="form-check-label" for="paao6">NC</label>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <input type="text" class="form-control m-l-30 h-70" id="aoremarks" placeholder="Your remarks here...">
                    </div>
                    <div class="form-group" id="2">
                        <label for="roremarks" class="fs-20">II. Registrar's Office</label>
                        <div class="form-row p-l-30">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">A. Quality Service Provided</label>
                                <label for="inputPassword4" class="m-b-10">B. Personal Attitude</label>
                            </div>
                            <div class="form-group col-md-6">
                                <form action="#" id="ro" class="m-b-10">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp1" value="1">
                                        <label class="form-check-label" for="roqsp1">1</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp2" value="2">
                                        <label class="form-check-label" for="roqsp2">2</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp3" value="3">
                                        <label class="form-check-label" for="roqsp3">3</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp4" value="4">
                                        <label class="form-check-label" for="roqsp4">4</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp5" value="5">
                                        <label class="form-check-label" for="roqsp5">5</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsRO" id="roqsp6" value="6">
                                        <label class="form-check-label" for="roqsp6">NC</label>
                                    </div>
                                </form>
                                <form action="#" id="ropaao">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao1" value="1">
                                        <label class="form-check-label" for="ropaao1">1</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao2" value="2">
                                        <label class="form-check-label" for="ropaao2">2</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao3" value="3">
                                        <label class="form-check-label" for="ropaao3">3</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao4" value="4">
                                        <label class="form-check-label" for="ropaao4">4</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao5" value="5">
                                        <label class="form-check-label" for="ropaao5">5</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptionsROPAAO" id="ropaao6" value="6">
                                        <label class="form-check-label" for="ropaao6">NC</label>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <input type="text" class="form-control m-l-30 h-70" id="roremarks" placeholder="Your remarks here...">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right" id="form-next">next</button>
            </div>
        </div>

        <a href="../api/post/logout.php" style="color: #FFF">Logout</a>
    </div>



    <!-- Bootstrap core JavaScript -->
    <script src="../resources/js/jquery-3.4.1.min.js"></script>
    <script src="../resources/js/popper.min.js"></script>
    <!-- <script src="resources/js/bootstrap.bundle.min.js"></script> -->

    <script src="../resources/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../resources/js/jquery.easing.min.js"></script>

    <!-- aos-js -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

</body>

</html>

<script>
    $("button#form-next").click(function() {
        debugger
        if (Object.entries(eval_remarks).length === 0 && eval_remarks.constructor === Object) {
            let counter = 0
            let checkers = $("#survey-container").children()
            if (checkers.length > 1) {
                for(let i = 0; i < checkers.length; i++){
                    while (counter < 2) {
                        let key = $(`input[name=inlineRadioOptions${_formNames[checkers[i].id][counter].toUpperCase()}]:checked`).attr('id')
                        let values = $(`input[name=inlineRadioOptions${_formNames[checkers[i].id][counter].toUpperCase()}]:checked`).val()
                        eval_remarks[key] = values
                        counter++
                    }
                    counter = 0
                }
                console.table(eval_remarks)
            }
        }
    })
</script>