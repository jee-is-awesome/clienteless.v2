<?php


    //necessary headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");

    include_once "../../config/Database.php";
    include_once "../login/Login.php";

    $db = new Database();
    $log = new Login($db->dbconn());

    $log->username = isset($_POST['user']) ? $_POST['user'] : die();
    $log->password = isset($_POST['pass']) ? $_POST['pass'] : die();
    $log->type = isset($_POST['default']) ? $_POST['default'] : die();

    $res = $log->checkCred();

    if($log->rowCount > 0){
        $user_cred = array(
            "id" => $log->studID,
            "year" => $log->studYear,
            "lastName" => $log->studLName,
            "firstName" => $log->studFName,
            "middleName" => $log->studMName,
            "course" => $log->studCourse,
            "campus" => $log->studCampus,
            "rowCount" => $log->rowCount
        );
        session_start();

        $_SESSION['userID'] = $log->studID;
        $_SESSION['fullName'] = $log->studFName." ".$log->studMName." ".$log->studLName;
    
        echo json_encode($user_cred);
    } else {
        echo json_encode(
            array(
                "message" => "Username or password is incorrect"
                )
            );
    }