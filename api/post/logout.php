<?php
// Initialize the session.
session_start();
// Finally, destroy the session.    
session_destroy();

// Include URL for Login page to login again.
header("Location: ../../index.php");
exit;
?>