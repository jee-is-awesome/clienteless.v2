<?php
class Login {
    
    private $con;

    //Login Properties
    public $username;
    public $password;
    public $type;

    //student properties
    public $studID;
    public $studYear;
    public $studLName;
    public $studFName;
    public $studMName;
    public $studCourse;
    public $studCampus;

    //row count
    public $rowCount;

    public $id;

    public function __construct($db){
        $this->con = $db;
    }

    public function checkCred(){
        $this->username = htmlspecialchars(strip_tags(($this->username)));
        $this->password = htmlspecialchars(strip_tags($this->password));
        $this->type = $this->type;

        if($this->type == 3){
            $query = "SELECT * FROM student WHERE studUsername = ? AND studPassword = ?";
        } else {
            return false;
        }

       

       $stmt = $this->con->prepare($query);

       $stmt->bindParam(1, $this->username);
       $stmt->bindParam(2, $this->password);

       $stmt->execute();

       $row = $stmt->fetch(PDO::FETCH_ASSOC);

       $this->studID = $row['studID'];
       $this->studYear = $row['studentryyear'];
       $this->studLName = $row['studLName'];
       $this->studFname = $row['studFName'];
       $this->studMName = $row['studMname'];
       $this->studCourse = $row['studCourse'];
       $this->studCampus = $row['studcampus'];
       $this->rowCount = $stmt->rowCount();

    }

}
